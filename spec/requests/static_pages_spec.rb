require 'rails_helper'
include ApplicationHelper

describe "Static pages" do
  subject {page}

  shared_examples_for "all static pages" do
    it {should have_content(heading)}
    it {should have_title(full_title(page_title))}
  end

  describe "Home page" do
    before { visit root_path }

    let(:heading) {'Sample App'}
    let(:page_title) {''}
    it_should_behave_like "all static pages"

    it { should_not have_title('| Home') }

    describe "for sigined-in user" do
      let(:user) {FactoryGirl.create(:user)}
      before do
        FactoryGirl.create(:micropost, user: user, content: 'Lorem ipsum')
        FactoryGirl.create(:micropost, user: user, content: 'Dolor sit amet')
        sign_in user
        visit root_path
      end

      it "should render the user's feed" do
        user.feed.each do |item|
          expect(page).to have_selector("li##{item.id}", text: item.content)
        end
      end
    end
  end

  describe "Help page" do
    before { visit help_path }
    it { should have_content('Help') }
    it { should have_title('Sample App | Help') }
  end

  describe "About page" do
    before { visit about_path }
    it { should have_content('About') }
    it { should have_title('Sample App | About')}
  end

  describe "Contact page" do
    before { visit contact_path }
    it { should have_content('Contact') }
    it { should have_title('Sample App | Contact') }
  end

  describe "full_title" do
    it "should include the page title" do
      expect(full_title("foo")).to match(/foo/)
    end
    it "should include the base title" do
      expect(full_title("foo")).to match(/^Sample App/)
    end
    it "should not include a bar for the home page" do
      expect(full_title("")).not_to match(/\|/)
    end
  end
end